const people = [
    {
        "id": 0,
        "name": "Dat"
    },
    {
        "id": 1,
        "name": "Mira"
    },
    {
        "id": 2,
        "name": "Speedy"
    },
    {
        "id": 3,
        "name": "Lubos"
    },
    {
        "id": 4,
        "name": "Samo"
    },
    {
        "id": 5,
        "name": "Mara"
    }
]

const tasks = [
    {
        "id": 0,
        "userId": 0,
        "description": "adoptovat kocicku",
        "completed": false
    },
    {
        "id": 1,
        "userId": 0,
        "description": "udelat prezentaci na coroutines",
        "completed": true
    },
    {
        "id": 2,
        "userId": 1,
        "description": "vymenit zarovky",
        "completed": false
    },
    {
        "id": 3,
        "userId": 1,
        "description": "hodit telefon do kyblu vody",
        "completed": true
    },
    {
        "id": 4,
        "userId": 2,
        "description": "zavolat babicce",
        "completed": false
    },
    {
        "id": 5,
        "userId": 2,
        "description": "smazat tabuli",
        "completed": true
    },
    {
        "id": 6,
        "userId": 3,
        "description": "vyhubit pavouky",
        "completed": false
    },
    {
        "id": 7,
        "userId": 3,
        "description": "chytit Pikachu",
        "completed": true
    },
    {
        "id": 8,
        "userId": 4,
        "description": "naucit se cestinu",
        "completed": false
    },
    {
        "id": 9,
        "userId": 4,
        "description": "nakrajet kedlubny",
        "completed": true
    },
    {
        "id": 10,
        "userId": 5,
        "description": "pochvalit Camila",
        "completed": false
    },
    {
        "id": 11,
        "userId": 5,
        "description": "dopsat diplomku",
        "completed": true
    }
]

function makeToken() {
    var result = []
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < 10; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)))
    }
    return result.join('')
}

var express = require("express")

var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({extended: false})

var app = express()
app.listen(3000, () => {
    console.log("Server running on port 3000")
})

app.get('/people', (req, res) => {
    console.log("get /people")
    res.setHeader('Content-Type', 'application/json')
    res.status(200)
    res.end(JSON.stringify(people))
})

app.get('/people/:id', (req, res) => {
    let id = req.params.id
    console.log("get /people/" + id)
    const person = people.find(person => person.id.toString() === id.toString())
    if (person !== undefined) {
        res.setHeader('Content-Type', 'application/json')
        res.status(200)
        res.end(JSON.stringify(person))
    } else {
        res.sendStatus(404)
    }
})

app.get('/tasks', (req, res) => {
    console.log("get /tasks")
    res.setHeader('Content-Type', 'application/json')
    res.status(200)
    res.end(JSON.stringify(tasks))
})

app.get('/tasks/:userId', (req, res) => {
    let userId = req.params.userId
    console.log("get /tasks/" + userId)
    const userTasks = tasks.filter(task => task.userId.toString() === userId.toString())
    if (userTasks !== undefined) {
        res.setHeader('Content-Type', 'application/json')
        res.status(200)
        res.end(JSON.stringify(userTasks))
    } else {
        res.sendStatus(404)
    }
})

app.post('/people', jsonParser, (req, res) => {
    const body = req.body
    console.log("post /people")
    console.log(body)
    const newId = people.length
    people.push({
        "id": newId,
        "name": body.name
    })
    res.setHeader('Content-Type', 'application/json')
    res.sendStatus(200)
})
