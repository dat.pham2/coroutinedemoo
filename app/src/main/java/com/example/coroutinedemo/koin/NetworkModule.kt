package com.example.coroutinedemo.koin

import com.example.coroutinedemo.data.api.DemoApi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

private const val REQUEST_TIMEOUT_S = 30L
private const val OK_HTTP_LOGGER_INTERCEPTOR_NAME = "OK_HTTP_LOGGER_INTERCEPTOR"
private const val OK_HTTP_CLIENT_NAME = "OK_HTTP_CLIENT"

internal fun networkModule() = module {
    defineInterceptors(this)
    defineOkHttpClients(this)
    defineApiClients(this)
}

private fun defineInterceptors(module: Module) {
    module.apply {
        single<Interceptor>(named(OK_HTTP_LOGGER_INTERCEPTOR_NAME)) {
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        }
    }
}

private fun defineOkHttpClients(module: Module) {
    module.apply {
        single<OkHttpClient>(named(OK_HTTP_CLIENT_NAME)) {
            val builder = OkHttpClient.Builder()
                .connectTimeout(REQUEST_TIMEOUT_S, TimeUnit.SECONDS)
                .callTimeout(REQUEST_TIMEOUT_S, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT_S, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT_S, TimeUnit.SECONDS)
            builder.addInterceptor(get<Interceptor>(named(OK_HTTP_LOGGER_INTERCEPTOR_NAME)))
            builder.build()
        }
    }
}

private fun defineApiClients(module: Module) {
    module.apply {
        single<DemoApi> {
            val retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2:3000/")
                .client(get<OkHttpClient>(named(OK_HTTP_CLIENT_NAME)))
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build()
            retrofit.create(DemoApi::class.java)
        }
    }
}
