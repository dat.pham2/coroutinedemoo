package com.example.coroutinedemo.koin

import com.example.coroutinedemo.data.repository.DemoRepository
import com.example.coroutinedemo.presentation.viewmodel.AddPersonFragmentViewModel
import com.example.coroutinedemo.presentation.viewmodel.PeopleFragmentViewModel
import com.example.coroutinedemo.presentation.viewmodel.PersonFragmentViewModel
import com.example.coroutinedemo.presentation.viewmodel.TasksFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun appModules() = listOf(
    networkModule(),
    dataModule(),
    viewModelModule()
)

fun dataModule() = module {
    single { DemoRepository(api = get()) }
}

fun viewModelModule() = module {
    viewModel { PeopleFragmentViewModel(demoRepository = get()) }
    viewModel { (userId: Long) ->
        PersonFragmentViewModel(userId = userId, demoRepository = get())
    }
    viewModel { TasksFragmentViewModel(demoRepository = get()) }
    viewModel { AddPersonFragmentViewModel(demoRepository = get()) }
}
