package com.example.coroutinedemo.domain.model

data class TaskModel(
    val id: Long,
    val userId: Long,
    val description: String,
    val completed: Boolean,
)
