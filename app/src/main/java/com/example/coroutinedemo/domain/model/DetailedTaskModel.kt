package com.example.coroutinedemo.domain.model

data class DetailedTaskModel(
    val id: Long,
    val userId: Long,
    val personName: String,
    val description: String,
    val completed: Boolean,
)
