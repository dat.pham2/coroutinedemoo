package com.example.coroutinedemo.domain.model

data class PersonModel(
    val id: Long,
    val name: String,
)
