package com.example.coroutinedemo.data.api

import com.example.coroutinedemo.data.api.entity.PersonEntity
import com.example.coroutinedemo.data.api.entity.PostPersonEntity
import com.example.coroutinedemo.data.api.entity.TaskEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface DemoApi {
    @GET("/people")
    fun getPeople(): Single<List<PersonEntity>>

    @GET("/people/{id}")
    fun getPerson(@Path("id") id: Long): Single<PersonEntity>

    @POST("/people")
    fun addPerson(@Body person: PostPersonEntity): Completable

    @GET("/tasks")
    fun getTasks(): Single<List<TaskEntity>>

    @GET("/tasks/{userId}")
    fun getTasksForPerson(@Path("userId") userId: Long): Single<List<TaskEntity>>
}
