package com.example.coroutinedemo.data.api.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostPersonEntity(
    @Json(name = "name") val name: String,
)
