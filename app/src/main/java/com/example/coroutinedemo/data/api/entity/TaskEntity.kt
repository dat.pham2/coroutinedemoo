package com.example.coroutinedemo.data.api.entity

import com.example.coroutinedemo.domain.model.TaskModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TaskEntity(
    @Json(name = "id") val id: Long,
    @Json(name = "userId") val userId: Long,
    @Json(name = "description") val description: String,
    @Json(name = "completed") val completed: Boolean,
) {
    companion object {
        fun TaskEntity.toModel(): TaskModel {
            return TaskModel(
                id = this.id,
                userId = this.userId,
                description = this.description,
                completed = this.completed,
            )
        }
    }
}
