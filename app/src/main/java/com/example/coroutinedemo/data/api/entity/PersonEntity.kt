package com.example.coroutinedemo.data.api.entity

import com.example.coroutinedemo.domain.model.PersonModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PersonEntity(
    @Json(name = "id") val id: Long,
    @Json(name = "name") val name: String,
) {
    companion object {
        fun PersonEntity.toModel(): PersonModel {
            return PersonModel(
                id = this.id,
                name = this.name,
            )
        }
    }
}
