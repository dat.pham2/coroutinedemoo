package com.example.coroutinedemo.data.repository

import com.example.coroutinedemo.data.api.DemoApi
import com.example.coroutinedemo.data.api.entity.PersonEntity.Companion.toModel
import com.example.coroutinedemo.data.api.entity.PostPersonEntity
import com.example.coroutinedemo.data.api.entity.TaskEntity.Companion.toModel
import com.example.coroutinedemo.domain.model.PersonModel
import com.example.coroutinedemo.domain.model.TaskModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

class DemoRepository(private val api: DemoApi) {
    fun getPeople(): Single<List<PersonModel>> {
        return api.getPeople().map { people ->
            people.map { it.toModel() }
        }
    }

    fun getPerson(id: Long): Single<PersonModel> {
        return api.getPerson(id).map { it.toModel() }
    }

    fun addPerson(name: String): Completable {
        return api.addPerson(PostPersonEntity(name = name))
    }

    fun getTasks(): Single<List<TaskModel>> {
        return api.getTasks().map { tasks ->
            tasks.map { it.toModel() }
        }
    }

    fun getTasksForPerson(userId: Long): Single<List<TaskModel>> {
        return api.getTasksForPerson(userId).map { tasks ->
            tasks.map { it.toModel() }
        }
    }
}
