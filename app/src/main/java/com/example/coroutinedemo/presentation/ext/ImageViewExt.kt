package com.example.coroutinedemo.presentation.ext

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("urlCenterInside")
fun loadImageCenterInside(imageView: ImageView, url: String?) {
    if (url?.isNotEmpty() == true) {
        imageView.loadUrl(url)
    }
}

fun ImageView.loadUrl(url: String) {
    Glide.with(this).load(url).apply(RequestOptions.centerInsideTransform())
        .into(this)
}
