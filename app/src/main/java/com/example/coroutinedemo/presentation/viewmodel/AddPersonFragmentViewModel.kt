package com.example.coroutinedemo.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.coroutinedemo.data.repository.DemoRepository
import io.reactivex.rxjava3.schedulers.Schedulers

class AddPersonFragmentViewModel(
    private val demoRepository: DemoRepository
) : RxViewModel() {
    val name = MutableLiveData<String>()

    private val _personAddedEvent = MutableLiveData<PersonAddedEvent>()
    val personAddedEvent: LiveData<PersonAddedEvent> = _personAddedEvent

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> = _isLoading

    fun addPersonClicked() {
        name.value?.takeIf { it.isNotBlank() }?.let { safeName ->
            demoRepository.addPerson(safeName)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnSubscribe { _isLoading.postValue(true) }
                .doFinally { _isLoading.postValue(false) }
                .subscribe({
                    _personAddedEvent.postValue(PersonAddedEvent())
                }, { thr ->
                    Log.e(this.javaClass.name, "Failed to add new person", thr)
                })
                .addTo(disposeBag)
        }
    }

    class PersonAddedEvent
}
