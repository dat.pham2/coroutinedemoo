package com.example.coroutinedemo.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.example.coroutinedemo.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var navigationController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.main_host_fragment_container) as NavHostFragment
        navigationController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.fragment_people, R.id.fragment_tasks))

        bottom_navigation_view?.inflateMenu(R.menu.menu_bottom_navigation)
        bottom_navigation_view?.setupWithNavController(navigationController)
        NavigationUI.setupActionBarWithNavController(this, navigationController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigationController.navigateUp()
    }
}
