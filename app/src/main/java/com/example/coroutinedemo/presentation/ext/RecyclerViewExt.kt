package com.example.coroutinedemo.presentation.ext

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("adapter")
fun RecyclerView.setAdapterBindingAdapter(adapterToBeSet: RecyclerView.Adapter<*>) {
    adapter = adapterToBeSet
}
