package com.example.coroutinedemo.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.FragmentTasksBinding
import com.example.coroutinedemo.presentation.rvadapter.DetailedTasksAdapter
import com.example.coroutinedemo.presentation.viewmodel.TasksFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TasksFragment : Fragment() {
    private val viewModel: TasksFragmentViewModel by viewModel()
    private lateinit var adapter: DetailedTasksAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DataBindingUtil.inflate<FragmentTasksBinding>(
            inflater, R.layout.fragment_tasks, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        adapter = DetailedTasksAdapter(viewLifecycleOwner)
        binding.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadDetailedTasks()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }

    private fun observe() {
        viewModel.tasks.observe(viewLifecycleOwner, adapter)
        adapter.itemClicked.observe(viewLifecycleOwner, { })
    }
}
