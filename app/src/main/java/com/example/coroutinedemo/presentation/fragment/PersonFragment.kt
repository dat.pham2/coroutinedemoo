package com.example.coroutinedemo.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.coroutinedemo.presentation.viewmodel.PersonFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import androidx.navigation.fragment.navArgs
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.FragmentPersonBinding
import com.example.coroutinedemo.presentation.rvadapter.TasksAdapter

class PersonFragment : Fragment() {
    private val navArgs: PersonFragmentArgs by navArgs()
    private val viewModel: PersonFragmentViewModel by viewModel { parametersOf(navArgs.userId) }
    private lateinit var adapter: TasksAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DataBindingUtil.inflate<FragmentPersonBinding>(
            inflater, R.layout.fragment_person, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        adapter = TasksAdapter(viewLifecycleOwner)
        binding.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }

    private fun observe() {
        viewModel.tasks.observe(viewLifecycleOwner, adapter)
        adapter.itemClicked.observe(viewLifecycleOwner, { task ->
            Toast.makeText(requireContext(), task.description, Toast.LENGTH_SHORT).show()
        })
    }
}
