package com.example.coroutinedemo.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.coroutinedemo.data.repository.DemoRepository
import com.example.coroutinedemo.domain.model.DetailedTaskModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class TasksFragmentViewModel(
    private val demoRepository: DemoRepository
) : RxViewModel() {
    private val _tasks = MutableLiveData<List<DetailedTaskModel>>()
    val tasks: LiveData<List<DetailedTaskModel>> = _tasks

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> = _isLoading

    /**
     * - use viewModelScope.launch(Dispatchers.IO)
     * - ask for ideas
     * - showcase awaitAll()
     */
    fun loadDetailedTasks() {
        Single.zip(
            demoRepository.getPeople().subscribeOn(Schedulers.io()),
            demoRepository.getTasks().subscribeOn(Schedulers.io()),
        ) { people, tasks ->
            val detailedTasks = mutableListOf<DetailedTaskModel>()
            tasks.forEach { task ->
                val person = people.find { it.id == task.userId }
                detailedTasks.add(
                    DetailedTaskModel(
                        id = task.id,
                        userId = task.userId,
                        personName = person?.name ?: "unknown",
                        description = task.description,
                        completed = task.completed
                    )
                )
            }
            detailedTasks
        }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnSubscribe { _isLoading.postValue(true) }
            .doFinally { _isLoading.postValue(false) }
            .subscribe({
                _tasks.postValue(it)
            }, { thr ->
                Log.e(this.javaClass.name, "Failed to load detailed tasks", thr)
            })
            .addTo(disposeBag)
    }
}
