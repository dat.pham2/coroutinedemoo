package com.example.coroutinedemo.presentation.rvadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.ItemDetailedTaskBinding
import com.example.coroutinedemo.domain.model.DetailedTaskModel
import com.example.coroutinedemo.presentation.viewholder.DetailedTaskItemViewHolder

class DetailedTasksAdapter(
    private val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<DetailedTaskItemViewHolder>(), Observer<List<DetailedTaskModel>> {
    private var data: List<DetailedTaskModel>? = null

    val itemClicked = MutableLiveData<DetailedTaskModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailedTaskItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = DataBindingUtil.inflate<ItemDetailedTaskBinding>(
            layoutInflater,
            R.layout.item_detailed_task,
            parent,
            false
        )
        itemView.lifecycleOwner = this.lifecycleOwner
        return DetailedTaskItemViewHolder(itemView.root) { model ->
            itemClicked.postValue(model)
        }.apply {
            itemView.viewHolder = this
        }
    }

    override fun onBindViewHolder(holder: DetailedTaskItemViewHolder, position: Int) {
        data?.get(position)?.let { safeModel ->
            holder.setData(safeModel)
        }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onChanged(newData: List<DetailedTaskModel>?) {
        newData?.let {
            data = it
            notifyDataSetChanged()
        }
    }
}
