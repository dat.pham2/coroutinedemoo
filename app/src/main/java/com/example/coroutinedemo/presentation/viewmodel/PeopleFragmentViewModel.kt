package com.example.coroutinedemo.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.coroutinedemo.data.repository.DemoRepository
import com.example.coroutinedemo.domain.model.PersonModel
import io.reactivex.rxjava3.schedulers.Schedulers

class PeopleFragmentViewModel(
    private val demoRepository: DemoRepository
) : RxViewModel() {
    private val _people = MutableLiveData<List<PersonModel>>()
    val people: LiveData<List<PersonModel>> = _people

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> = _isLoading

    /**
     * coroutine: block of code meant to be executed asynchronously
     * job: implementation abstraction of a coroutine
     * suspend: methods with suspend keyword are meant to and can only be executed within a coroutine. A suspend
     * function (and the coroutine it is in) can be paused and resumed at a later time, by the thread it's running on.
     * Coroutine needs to be run inside a coroutineScope. [androidx.lifecycle.ViewModel] provides a default one.
     *
     * Todo:
     * - integrate suspend call to get people
     * - explain execution order and threading
     *    1. use default viewModelScope that uses main thread
     *    2. use launch with Dispatchers.IO
     *    3. showcase how to mimic .observeOn(AndroidSchedulers.mainThread())
     * - handle errors
     *    1. simple try-catch
     *    2. CoroutineExceptionHandler
     *       - Handlers provided to non-root coroutines of a scope will be ignored.
     *         A handler provided to the root coroutine will be executed. :(
     *         (use 2 handlers to illustrate)
     * - migration for .dispose()
     */
    fun loadPeople() {
        demoRepository.getPeople()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnSubscribe {
                _isLoading.postValue(true)
            }
            .doFinally { _isLoading.postValue(false) }
            .subscribe({
                _people.postValue(it)
            }, { thr ->
                Log.e(this.javaClass.name, "Failed to load people", thr)
            })
            .addTo(disposeBag)
    }
}
