package com.example.coroutinedemo.presentation.rvadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.ItemTaskBinding
import com.example.coroutinedemo.domain.model.TaskModel
import com.example.coroutinedemo.presentation.viewholder.TaskItemViewHolder

class TasksAdapter(
    private val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<TaskItemViewHolder>(), Observer<List<TaskModel>> {
    private var data: List<TaskModel>? = null

    val itemClicked = MutableLiveData<TaskModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = DataBindingUtil.inflate<ItemTaskBinding>(
            layoutInflater,
            R.layout.item_task,
            parent,
            false
        )
        itemView.lifecycleOwner = this.lifecycleOwner
        return TaskItemViewHolder(itemView.root) { model ->
            itemClicked.postValue(model)
        }.apply {
            itemView.viewHolder = this
        }
    }

    override fun onBindViewHolder(holder: TaskItemViewHolder, position: Int) {
        data?.get(position)?.let { safeModel ->
            holder.setData(safeModel)
        }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onChanged(newData: List<TaskModel>?) {
        newData?.let {
            data = it
            notifyDataSetChanged()
        }
    }
}
