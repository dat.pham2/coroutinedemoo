package com.example.coroutinedemo.presentation.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class RxViewModel : ViewModel() {
    protected val disposeBag = CompositeDisposable()

    fun dispose() {
        disposeBag.dispose()
    }
}
