package com.example.coroutinedemo.presentation.ext

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGoneBindingAdapter(value: Boolean?) {
    visibility = if (value == true) View.VISIBLE else View.GONE
}
