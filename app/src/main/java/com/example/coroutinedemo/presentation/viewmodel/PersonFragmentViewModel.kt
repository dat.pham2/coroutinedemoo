package com.example.coroutinedemo.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.coroutinedemo.data.repository.DemoRepository
import com.example.coroutinedemo.domain.model.PersonModel
import com.example.coroutinedemo.domain.model.TaskModel
import io.reactivex.rxjava3.schedulers.Schedulers

class PersonFragmentViewModel(
    private val userId: Long,
    private val demoRepository: DemoRepository
) : RxViewModel() {
    private val _person = MutableLiveData<PersonModel>()
    val person: LiveData<PersonModel> = _person

    private val _tasks = MutableLiveData<List<TaskModel>>()
    val tasks: LiveData<List<TaskModel>> = _tasks

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> = _isLoading

    /**
     * - showcase execution order in coroutine vs flatMap
     * - simplification for observeOn(Schedulers.io()), i.e. skipping inner coroutine
     */
    fun loadData() {
        demoRepository.getPerson(userId)
            .flatMap { person ->
                _person.postValue(person)
                demoRepository.getTasksForPerson(userId)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnSubscribe { _isLoading.postValue(true) }
            .doFinally { _isLoading.postValue(false) }
            .subscribe({ tasks ->
                _tasks.postValue(tasks)
            }, { thr ->
                Log.e(this.javaClass.name, "Failed to load person data", thr)
            })
            .addTo(disposeBag)

    }
}
