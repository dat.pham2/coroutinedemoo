package com.example.coroutinedemo.presentation.viewholder

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinedemo.domain.model.PersonModel

class PersonItemViewHolder(
    view: View,
    private val onClick: (PersonModel) -> Unit
) : RecyclerView.ViewHolder(view) {
    private val _person = MutableLiveData<PersonModel>()
    val person: LiveData<PersonModel> = _person

    fun onClick() {
        _person.value?.let { onClick.invoke(it) }
    }

    fun setData(person: PersonModel) {
        _person.postValue(person)
    }
}
