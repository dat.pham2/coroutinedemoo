package com.example.coroutinedemo.presentation.viewholder

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinedemo.domain.model.TaskModel

class TaskItemViewHolder(
    view: View,
    private val onClick: (TaskModel) -> Unit
) : RecyclerView.ViewHolder(view) {
    private val _task = MutableLiveData<TaskModel>()
    val task: LiveData<TaskModel> = _task

    fun onClick() {
        _task.value?.let { onClick.invoke(it) }
    }

    fun setData(task: TaskModel) {
        _task.postValue(task)
    }
}
