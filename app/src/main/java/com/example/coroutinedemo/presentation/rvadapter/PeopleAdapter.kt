package com.example.coroutinedemo.presentation.rvadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.ItemPersonBinding
import com.example.coroutinedemo.domain.model.PersonModel
import com.example.coroutinedemo.presentation.viewholder.PersonItemViewHolder

class PeopleAdapter(
    private val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<PersonItemViewHolder>(), Observer<List<PersonModel>> {
    private var data: List<PersonModel>? = null

    val itemClicked = MutableLiveData<PersonModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = DataBindingUtil.inflate<ItemPersonBinding>(
            layoutInflater,
            R.layout.item_person,
            parent,
            false
        )
        itemView.lifecycleOwner = this.lifecycleOwner
        return PersonItemViewHolder(itemView.root) { model ->
            itemClicked.postValue(model)
        }.apply {
            itemView.viewHolder = this
        }
    }

    override fun onBindViewHolder(holder: PersonItemViewHolder, position: Int) {
        data?.get(position)?.let { safeModel ->
            holder.setData(safeModel)
        }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onChanged(newData: List<PersonModel>?) {
        newData?.let {
            data = it
            notifyDataSetChanged()
        }
    }
}
