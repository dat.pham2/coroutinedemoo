package com.example.coroutinedemo.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.FragmentPeopleBinding
import com.example.coroutinedemo.presentation.rvadapter.PeopleAdapter
import com.example.coroutinedemo.presentation.viewmodel.PeopleFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PeopleFragment : Fragment() {
    private val viewModel: PeopleFragmentViewModel by viewModel()
    private lateinit var adapter: PeopleAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DataBindingUtil.inflate<FragmentPeopleBinding>(
            inflater, R.layout.fragment_people, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        adapter = PeopleAdapter(viewLifecycleOwner)
        binding.adapter = adapter
        binding.btnAddPerson.setOnClickListener {
            findNavController().navigate(PeopleFragmentDirections.toAddPerson())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadPeople()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }

    private fun observe() {
        viewModel.people.observe(viewLifecycleOwner, adapter)
        adapter.itemClicked.observe(viewLifecycleOwner, { person ->
            findNavController().navigate(PeopleFragmentDirections.toPerson(person.id))
        })
    }
}
