package com.example.coroutinedemo.presentation.viewholder

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutinedemo.domain.model.DetailedTaskModel

class DetailedTaskItemViewHolder(
    view: View,
    private val onClick: (DetailedTaskModel) -> Unit
) : RecyclerView.ViewHolder(view) {
    private val _task = MutableLiveData<DetailedTaskModel>()
    val task: LiveData<DetailedTaskModel> = _task

    fun onClick() {
        _task.value?.let { onClick.invoke(it) }
    }

    fun setData(task: DetailedTaskModel) {
        _task.postValue(task)
    }
}
