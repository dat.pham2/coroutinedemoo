package com.example.coroutinedemo.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.coroutinedemo.R
import com.example.coroutinedemo.databinding.FragmentAddPersonBinding
import com.example.coroutinedemo.presentation.viewmodel.AddPersonFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddPersonFragment : Fragment() {
    private val viewModel: AddPersonFragmentViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DataBindingUtil.inflate<FragmentAddPersonBinding>(
            inflater, R.layout.fragment_add_person, container, false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }

    private fun observe() {
        viewModel.personAddedEvent.observe(viewLifecycleOwner, {
            findNavController().navigateUp()
        })
    }
}
